This is the README.txt file for the granny project.

Required patches:

2287073-89.patch
https://www.drupal.org/node/2287073

2697587-48.patch
https://www.drupal.org/node/2697587

source_plugins_have_a-2560795-64.patch
https://www.drupal.org/node/2560795


Running the migration
=====================

Export a Backup file from Gramps for the tree of interest. This file
should only have the XML, not the media.

The file should be named data.gramps.

Put the file into the /tmp directory.

Run migrations in the following order using drush:


drush mi tags
drush mi notes
drush mi repositories
drush mi sources
drush mi citations
drush mi places
drush mi events
drush mi people
drush mi families

Or just run everything:

drush mi families --execute-dependencies


Things currently not yet being imported
=======================================

- Addresses
- Notes and Citations on Attributes
- Media
- LDS stuff
