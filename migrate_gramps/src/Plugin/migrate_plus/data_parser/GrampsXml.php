<?php

namespace Drupal\migrate_gramps\Plugin\migrate_plus\data_parser;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateException;
use Drupal\migrate_plus\DataParserPluginBase;

/**
 * Obtain Gramps XML data for migration.
 *
 * @DataParser(
 *   id = "grampsxml",
 *   title = @Translation("Gramps XML")
 * )
 */
class GrampsXml extends DataParserPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Array of matches from item_selector.
   */
  protected $matches = [];

  protected $counter;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    // Suppress errors during parsing, so we can pick them up after.
    libxml_use_internal_errors(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  protected function openSourceUrl($url) {
    if (!strpos($url, '.gramps')) {
      throw new MigrateException('Filename is not a .gramps file.');
      return FALSE;
    }
    $xml = simplexml_load_file($url);
    $this->registerNamespaces($xml);
    $xpath = $this->configuration['item_selector'];
    $this->matches = $xml->xpath($xpath);
    $this->counter = 0;
    foreach (libxml_get_errors() as $error) {
      $error_string = self::parseLibXmlError($error);
      throw new MigrateException($error_string);
      return FALSE;
    }
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function fetchNextRow() {
    foreach ($this->matches as $index => $target_element) {
      if ($index == $this->counter) {
        if ($target_element) {
          $this->registerNamespaces($target_element);
          foreach ($this->fieldSelectors() as $field_name => $xpath) {
            if (strpos($xpath, 'evaluate|') === 0) {
              // apply a function. SimpleXML does not support this.
              $expression =  explode('|', $xpath);
              $value = $target_element->xpath($expression[2]);
              if (strpos($expression[1], '->') === 0) {
                $expression = substr($expression[1], 2);
                $this->currentItem[$field_name] = $value->$expression;
              }
              else {
                $this->currentItem[$field_name] = $expression[1]($value);
              }
            }
            else {
              $match = $target_element->xpath($xpath);

              foreach ($match as $ind => $m) {
                $this->currentItem[$field_name][] = $this->recurseSimpleXmlElement($m);
              }
            }
            // flatten the resulting array
            if (count($this->currentItem[$field_name]) == 1 && !is_array(reset($this->currentItem[$field_name]))) {
              $this->currentItem[$field_name] = reset($this->currentItem[$field_name]);
            }
          }
        }
      }
    }

    $this->counter++;
  }

  /**
   * Registers the iterator's namespaces to a SimpleXMLElement.
   *
   * @param \SimpleXMLElement $xml
   *   The element to apply namespace registrations to.
   */
  protected function registerNamespaces(\SimpleXMLElement $xml) {
    if (is_array($this->configuration['namespaces'])) {
      foreach ($this->configuration['namespaces'] as $prefix => $ns) {
        $xml->registerXPathNamespace($prefix, $ns);
      }
    }
  }

  /**
   * Parses a SimpleXMLElement to an array.
   *
   * @param \SimpleXMLElement $xml
   *   XML element.
   *
   * @return array
   *   Array
   */
  protected function recurseSimpleXmlElement(\SimpleXMLElement $xml) {
    $values = array();
    if ($attributes = $xml->attributes()) {
      foreach ($attributes as $key => $att) {
        $values[$key] = $att->__toString();
      }
    }
    // $xml->children() always returns TRUE.
    if ($xml->count() && $children = $xml->children()) {
      foreach ($children as $child) {
        if ($child->children() || $child->attributes()) {
          $values[$child->getName()][] = $this->recurseSimpleXmlElement($child);
          #        var_dump($values[$child->getName()], $child);
        }
        else {
          #        var_dump('children, no grandchildren', $child->getName());
          $values[$child->getName()] = $child->__toString();
        }
      }
    }
    else {
      $str = $xml->__toString();
      if (!is_null($str) && $str !== '') {
        $values[$xml->getName()] = $str;
      }
      #    var_dump('no children', $xml->getName(), $xml->__toString());
    }

    // this is for elements that only have a single entry, most likely from attributes.
    if (count($values) == 1) {
      $key = key($values);
      $values = $values[$key];
    }
    return $values;
  }

  /**
   * Parses a LibXMLError to a error message string.
   *
   * @param \LibXMLError $error
   *   Error thrown by the XML.
   *
   * @return string
   *   Error message
   */
  public static function parseLibXmlError(\LibXMLError $error) {
    $error_code_name = 'Unknown Error';
    switch ($error->level) {
      case LIBXML_ERR_WARNING:
        $error_code_name = t('Warning');
        break;

      case LIBXML_ERR_ERROR:
        $error_code_name = t('Error');
        break;

      case LIBXML_ERR_FATAL:
        $error_code_name = t('Fatal Error');
        break;
    }

    return t(
      "@libxmlerrorcodename @libxmlerrorcode: @libxmlerrormessage\n" .
      "Line: @libxmlerrorline\n" .
      "Column: @libxmlerrorcolumn\n" .
      "File: @libxmlerrorfile",
      [
        '@libxmlerrorcodename' => $error_code_name,
        '@libxmlerrorcode' => $error->code,
        '@libxmlerrormessage' => trim($error->message),
        '@libxmlerrorline' => $error->line,
        '@libxmlerrorcolumn' => $error->column,
        '@libxmlerrorfile' => (($error->file)) ? $error->file : NULL,
      ]
    );
  }

}
