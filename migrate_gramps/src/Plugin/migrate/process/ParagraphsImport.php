<?php

/**
 * @file
 * Contains \Drupal\migrate_gramps\Plugin\migrate\process\ParagraphsImport.
 */

namespace Drupal\migrate_gramps\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;

/**
 *
 * @MigrateProcessPlugin(
 *   id = "paragraphs_import",
 * )
 */
class ParagraphsImport extends ProcessPluginBase {
  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (!isset($this->configuration['paragraph_type'])) {
      throw new MigrateException('Specify a paragraph type.');
    }
    if (!isset($this->configuration['fields'])) {
      throw new MigrateException('Give fields in same order as source values.');
    }
    $base = FALSE;
    if (isset($this->configuration['base'])) {
      $base = $this->configuration['base'];
    }
    if ($value && is_array($value)) {
      $paragraph_values = array();
      if (!$base) {
        foreach ($this->configuration['fields'] as $idx => $field) {
          foreach ($field as $field_name => $source_field) {
            // only populate fields with values.
            if (isset($value[$source_field]) && !is_null($value[$source_field])) {
              // extra handling for sub-elements in some forms like "link"
              if (strpos($field_name, '/') !== FALSE) {
                $field_name_array = explode('/', $field_name);
                if ($field_name_array[1] == "ERR") {
                  if (isset($value[$source_field]) && count($value[$source_field])) {
                    $paragraph_values[$field_name_array[0]] = $value[$source_field];
                  }

                }
                else {
                  $paragraph_values[$field_name_array[0]][$field_name_array[1]] = $value[$source_field];
                }
              }
              else {
                $paragraph_values[$field_name]['value'] = $value[$source_field];
              }
            }
          }
        }
      }
      else if (isset($value[$base]) && is_array($value[$base])) {
        foreach ($value[$base] as $idx => $property) {
          foreach ($this->configuration['fields'] as $fidx => $field) {
            foreach ($field as $field_name => $source_field) {
              if (isset($property[$source_field])) {
                if (strpos($field_name, '/') !== FALSE) {
                  $field_name_array = explode('/', $field_name);
                  $paragraph_values[$idx][$field_name_array[0]][$field_name_array[1]] = $value[$base][$idx][$source_field];
                }
                else {
                  $paragraph_values[$idx][$field_name]['value'] = $value[$base][$idx][$source_field];
                }
              }
            }
          }
        }
      }
      else if ($base && !is_array($value[$base])) {
        foreach ($this->configuration['fields'] as $fidx => $field) {
          foreach ($field as $field_name => $source_field) {
            if ($source_field == $base) {
              $paragraph_values[0][$field_name]['value'] = $value[$base];
            }
          }
        }
      }
      // don't create empty paragraphs
      if (!$base && count($paragraph_values)) {
        $paragraph_values = array_merge(
          array(
            'id' => NULL,
            'type' => $this->configuration['paragraph_type'],
          ),
          $paragraph_values);
        $paragraph = Paragraph::create($paragraph_values);
        $paragraph->save();
        return $paragraph;
      }
      else if ($base && count($paragraph_values)) {
        $paragraphs = array();
        foreach ($paragraph_values as $idx => $p) {
          $p = array_merge(
            array(
              'id' => NULL,
              'type' => $this->configuration['paragraph_type'],
            ),
            $p);

          $paragraph = Paragraph::create($p);
          $success = $paragraph->save();
          if ($success && !is_null($paragraph)) {
            $paragraphs[] = array(
              'target_id' => $paragraph->id(),
              'target_revision_id' => $paragraph->getRevisionId(),
            );
          }
        }

        $value[$base] = $paragraphs;

        return $value;
      }
      // pass through values
      else if ($base) {
        return $value;
      }
    }
  }
}
