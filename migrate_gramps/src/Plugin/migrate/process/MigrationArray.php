<?php

/**
 * @file
 * Contains \Drupal\migrate_gramps\Plugin\migrate\process\MigrationArray.
 */

namespace Drupal\migrate_gramps\Plugin\migrate\process;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
#use Drupal\migrate\MigrateSkipProcessException;
use Drupal\migrate\Plugin\MigratePluginManager;
use Drupal\migrate\Plugin\MigrationPluginManagerInterface;
use Drupal\migrate\Plugin\MigrateIdMapInterface;
use Drupal\migrate\Plugin\migrate\process\Migration;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Plugin\MigrationInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\MigrateException;
use Drupal\migrate\Row;

/**
 * Calculates the value of a property based on a previous migration.
 *
 * Accepts an array as input.
 *
 * @MigrateProcessPlugin(
 *   id = "migrationarray"
 * )
 */
class MigrationArray extends Migration implements ContainerFactoryPluginInterface {
  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (!isset($this->configuration['array_key'])) {
      throw new MigrateException('Give an array key for the element of the array to use.');
    }
    $array_key = $this->configuration['array_key'];
    if (isset($value) && !is_array($value)) {
      $old_value = $value;
      $value = array();
      $value[$array_key] = $old_value;
    }
    $search_value = FALSE;
    if (strpos($array_key, '/') !== FALSE) {
      $array_key_split = explode('/', $array_key);
      if (isset($value[$array_key_split[0]])) {
        if (isset($value[$array_key_split[0]][$array_key_split[1]])) {
          $search_value = $value[$array_key_split[0]][$array_key_split[1]];
        }
        else {
          foreach ($value[$array_key_split[0]] as $idx => $input) {
            if (is_array($input)) {
              if (isset($input[$array_key_split[1]]) && count($input[$array_key_split[1]]) == 1) {
              }
            }
          }
        }
      }
      else if (isset($value[$array_key_split[0]])) {
        $search_value = $value[$array_key_split[0]][$array_key_split[1]];
      }
    }
    else if (isset($value[$array_key])) {
      $search_value = $value[$array_key];
    }
    if ($search_value && $result = parent::transform($search_value, $migrate_executable, $row, $destinationProperty)) {
      // Flatten result
      if (is_array($result) && count($result) == 1) {
        $result = reset($result);
      }
      if (isset($array_key_split)) {
        // this changes ie citationref/hlink to citationref only
        unset($value[$array_key_split[0]]);
        $value[$array_key_split[0]] = $result;
      }
      else {
        $value[$array_key] = $result;
      }

      return $value;
    }
    else {
      return $value;
    }
  }
}
