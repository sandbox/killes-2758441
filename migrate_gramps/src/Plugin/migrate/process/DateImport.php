<?php

/**
 * @file
 * Contains \Drupal\migrate_gramps\Plugin\migrate\process\DateImport.
 */

namespace Drupal\migrate_gramps\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 *
 * @MigrateProcessPlugin(
 *   id = "date_import",
 * )
 */
class DateImport extends ProcessPluginBase {
  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if ($value && is_array($value)) {
      if ($value[0]) {
        if (is_array($value[0])) {
          $value[0][0]['date'] = $value[0][0]['dateval'];
          $value[0][0]['datetype'] = 'normal';
          return $value[0][0];
        }
        else {
          return [
            'date' => $value[0],
            'date_type' => 'normal',
          ];
        }
      }
      else if ($value[1] || $value[2]) {
        $value[5][0]['datetype'] = 'daterange';
        $value[5][0]['date'] = $value[5][0]['start'];
        $value[5][0]['end_date'] = $value[5][0]['stop'];
        return $value[5][0];
      }
      else if ($value[3] || $value[4]) {
        $value[6][0]['datetype'] = 'datespan';
        $value[6][0]['date'] = $value[5][0]['start'];
        $value[6][0]['end_date'] = $value[5][0]['stop'];
        return $value[6][0];
      }
    }
  }
}
